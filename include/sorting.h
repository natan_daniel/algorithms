/*
 * sorting.h
 *
 *  Created on: Jul 14, 2017
 *      Author: root
 */

#ifndef INCLUDE_SORTING_H_
#define INCLUDE_SORTING_H_

void insertionSort(int array[], int arraySize);

void recursiveInsertionSort(int array[], int arraySize);


#endif /* INCLUDE_SORTING_H_ */
