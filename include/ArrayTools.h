/*
 * ArrayTools.h
 *
 *  Created on: Jul 15, 2017
 *      Author: Natan
 */

#ifndef INCLUDE_ARRAYTOOLS_H_
#define INCLUDE_ARRAYTOOLS_H_

/* Function to print an array */
void printArray(int arr[], int size);

#endif /* INCLUDE_ARRAYTOOLS_H_ */
