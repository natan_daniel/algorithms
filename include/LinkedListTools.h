/*
 * LinkedListTools.h
 *
 *  Created on: Jul 15, 2017
 *      Author: Natan
 */

#ifndef INCLUDE_LINKEDLISTTOOLS_H_
#define INCLUDE_LINKEDLISTTOOLS_H_

/* A list is made of nodes, each node pointing to the next node. The tail node points to NULL. */

struct Node {
	int data;
	struct Node *next;
};

/* A utility function to insert a node at the beginning of linked list. */
void push(struct Node** pointerToRootNodePointer, int newRootNodeValue);

/* A utility function to print linked list. */
void printLinkedList(struct Node* node);

/* Returns the last node of the list. */
struct Node* getTail(struct Node* node);

#endif /* INCLUDE_LINKEDLISTTOOLS_H_ */
