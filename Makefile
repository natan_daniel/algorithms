CXXFLAG=-O2 -g -Wall -fmessage-length=0

ROOT=$(shell pwd)
SRC=$(ROOT)/src
LIB=$(ROOT)/lib
BIN=$(ROOT)/bin
INC=$(ROOT)/include
OBJ=$(ROOT)/obj

LDFLAGS= -lm -lpthread -lX11

directories: ${OBJ} ${BIN} ${LIB}

${OBJ}:
	mkdir ${OBJ}

${BIN}:
	mkdir ${BIN}

${LIB}:
	mkdir ${LIB}

# regles generales:

$(OBJ)/%.o: $(SRC)/%.cpp
	$(CXX) -c -o $@ $< -I$(INC)

$(BIN)/%: $(OBJ)/%.o
	$(CXX) -o $@ $< $(LDFLAGS)

#fin regles generales

all: directories $(BIN)/InsertionSortArray $(BIN)/QuickSortArray $(BIN)/QuickSortLinkedList

clean:
	rm -f ${OBJ}/* ${BIN}/*

# InsertionSort

$(BIN)/InsertionSortArray: $(OBJ)/InsertionSortArray.o $(OBJ)/sorting.o
	$(CXX) -o $@ $^ $(LDFLAGS)
	
InsertionSortArray: $(OBJ)/InsertionSortArray.o
	
# Quicksort

$(BIN)/QuickSortArray: $(OBJ)/QuickSortArray.o $(OBJ)/ArrayTools.o
	$(CXX) -o $@ $^ $(LDFLAGS)	
	
QuickSortArray: $(OBJ)/QuickSortArray.o
ArrayTools: $(OBJ)/ArrayTools.o

$(BIN)/QuickSortLinkedList: $(OBJ)/QuickSortLinkedList.o $(OBJ)/LinkedListTools.o
	$(CXX) -o $@ $^ $(LDFLAGS)	
	
QuickSortLinkedList: $(OBJ)/QuickSortLinkedList.o
LinkedListTools: $(OBJ)/LinkedListTools.o





