#include <stdio.h>

#include "LinkedListTools.h"

/* A utility function to insert a node at the beginning of linked list. */

void push(struct Node** pointerToRootNodePointer, int newRootNodeValue) {

	// This pointer refers to the node that will be used as the new head or root of the linked list
	struct Node* newRootNodePointer = new Node;

	// Setting the value of new root node using the new root node pointer
	newRootNodePointer->data = newRootNodeValue;

	// Setting our new root node's pointer to refer to the previous root node.
	// To do that we use our pointer to the previous root node pointer and access the value it points to, which is the previous root node pointer.
	newRootNodePointer->next = (*pointerToRootNodePointer);

	// Now we update the pointer to the root node pointer by telling it to point to the new root node pointer.
	(*pointerToRootNodePointer) = newRootNodePointer;
}

/* A utility function to print linked list. */

void printLinkedList(struct Node* node) {

	while (node != NULL) {
		printf(" %d ", node->data);

		node = node->next;
	}
}

/* Returns the last node of the list. */

struct Node* getTail(struct Node* node) {

	while (node != NULL && node->next != NULL) {
		node = node->next;
	}
	return node;
}
