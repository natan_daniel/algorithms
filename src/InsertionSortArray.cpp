//============================================================================
// Name        : Algorithms.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C, Ansi-style
//============================================================================

#include <stdio.h>
#include <stdlib.h>
#include "sorting.h"
#include <time.h>

int main(void) {

	int array[10] = { 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 };

	printf("Test 1 : \n");
	for (int i = 0; i < 10; i++) {
		printf(" %d |", array[i]);
	}

	printf("\n\n");

	insertionSort(array, 10);

	printf("Insertion Sort : \n");

	for (int i = 0; i < 10; i++) {
		printf(" %d |", array[i]);
	}

	printf("\n\n");

	printf("Recursive Insertion Sort : \n");

	recursiveInsertionSort(array, 10);

	for (int i = 0; i < 10; i++) {
		printf(" %d |", array[i]);
	}

	printf("\n\n");

	srand(time(NULL));

	for (int i = 0; i < 10; i++)
		array[i] = (rand()%10)+1;

	printf("Test 2 : \n");
	for (int i = 0; i < 10; i++) {
		printf(" %d |", array[i]);
	}

	printf("\n\n");

	insertionSort(array, 10);

	printf("Insertion Sort : \n");

	for (int i = 0; i < 10; i++) {
		printf(" %d |", array[i]);
	}

	printf("\n\n");

	printf("Recursive Insertion Sort : \n");

	recursiveInsertionSort(array, 10);

	for (int i = 0; i < 10; i++) {
		printf(" %d |", array[i]);
	}

	printf("\n\n");

	return EXIT_SUCCESS;
}
