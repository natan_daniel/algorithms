/*
 * insertionSort.cpp
 *
 *  Created on: Jul 14, 2017
 *      Author: Natan
 *
 */

#include "sorting.h"

#include <iostream>
#include <cstdio>

using namespace std;

/*** ARRAYS ***/

void insertElement1(int array[], int eltToPlace, int n) {

	int j;

	for (j = n - 1; j >= 0 && eltToPlace < array[j]; j--) {

		array[j + 1] = array[j];

		if (j == 0)
			array[j] = eltToPlace;
	}

	if (j != (n - 1))
		array[j + 1] = eltToPlace;
}

void insertionSort(int array[], int arraySize) {

	int i;

	for (i = 1; i < arraySize; i++) {

		insertElement1(array, array[i], i);
	}
}

void recursiveInsertionSort(int array[], int n) {

	while (n > 1) {
		recursiveInsertionSort(array, n - 1);
		insertElement1(array, array[n - 1], n - 1);
		n--;
	}
}


